module Sounds

open Fable.Core
open Fable.Core.JsInterop

type ButtonPosition =
    | Left
    | Middle
    | Right

type SoundObject =
    { Name: string
      ImageFile: string
      Audio: Browser.Types.HTMLAudioElement
      mutable IsPlaying: bool
      Position: ButtonPosition }


[<Emit("new Audio($0)")>]
let newAudio (_path: string) : Browser.Types.HTMLAudioElement = jsNative

let audioSourceDir = "../assets/sounds/"
let imageSourceDir = "../assets/images/"

printfn "%A" audioSourceDir
printfn "%A" imageSourceDir

let mutable dejaVu =
    { Name = "dejaVu"
      ImageFile = (imageSourceDir + "logo-sncf.png")
      Audio = newAudio (audioSourceDir + "deja_vu.mp3")
      IsPlaying = false
      Position = Middle }

let mutable sncf =
    { Name = "sncf"
      ImageFile = (imageSourceDir + "logo-sncf.png")
      Audio = newAudio (audioSourceDir + "sncf.mp3")
      IsPlaying = false
      Position = Left }

let mutable ah =
    { Name = "ah"
      ImageFile = (imageSourceDir + "ah.png")
      Audio = newAudio (audioSourceDir + "ah.mp3")
      IsPlaying = false
      Position = Middle }

let mutable toca =
    { Name = "toca"
      ImageFile = (imageSourceDir + "minions.png")
      Audio = newAudio (audioSourceDir + "tocata.mp3")
      IsPlaying = false
      Position = Right }

let mutable tac =
    { Name = "tac"
      ImageFile = (imageSourceDir + "clav.png")
      Audio = newAudio (audioSourceDir + "tac.mp3")
      IsPlaying = false
      Position = Left }

let mutable bull =
    { Name = "bull"
      ImageFile = (imageSourceDir + "bulle.png")
      Audio = newAudio (audioSourceDir + "bull.mp3")
      IsPlaying = false
      Position = Middle }

let mutable sonnette =
    { Name = "sonnette"
      ImageFile = (imageSourceDir + "sonnette.png")
      Audio = newAudio (audioSourceDir + "sonette.mp3")
      IsPlaying = false
      Position = Right }

let mutable tombo =
    { Name = "tombo"
      ImageFile = (imageSourceDir + "tambour.png")
      Audio = newAudio (audioSourceDir + "tombo.mp3")
      IsPlaying = false
      Position = Left }

let mutable bol =
    { Name = "bol"
      ImageFile = (imageSourceDir + "bol.png")
      Audio = newAudio (audioSourceDir + "bol.mp3")
      IsPlaying = false
      Position = Middle }

let mutable marche =
    { Name = "marche"
      ImageFile = (imageSourceDir + "marche.png")
      Audio = newAudio (audioSourceDir + "marche.mp3")
      IsPlaying = false
      Position = Right }

let mutable av =
    { Name = "av"
      ImageFile = (imageSourceDir + "avengers.png")
      Audio = newAudio (audioSourceDir + "avengers.mp3")
      IsPlaying = false
      Position = Left }

let mutable non =
    { Name = "non"
      ImageFile = (imageSourceDir + "non.png")
      Audio = newAudio (audioSourceDir + "gotaga.mp3")
      IsPlaying = false
      Position = Middle }

let mutable coucou =
    { Name = "coucou"
      ImageFile = (imageSourceDir + "coucou.png")
      Audio = newAudio (audioSourceDir + "coucou.mp3")
      IsPlaying = false
      Position = Right }

let mutable temp =
    { Name = "temp"
      ImageFile = (imageSourceDir + "temp.png")
      Audio = newAudio (audioSourceDir + "tempate.mp3")
      IsPlaying = false
      Position = Left }

let mutable rob =
    { Name = "rob"
      ImageFile = (imageSourceDir + "roblox.png")
      Audio = newAudio (audioSourceDir + "roblox.mp3")
      IsPlaying = false
      Position = Middle }

let mutable prout =
    { Name = "prout"
      ImageFile = (imageSourceDir + "prout.png")
      Audio = newAudio (audioSourceDir + "prout.mp3")
      IsPlaying = false
      Position = Right }

let mutable gifi =
    { Name = "gifi"
      ImageFile = (imageSourceDir + "gifi.png")
      Audio = newAudio (audioSourceDir + "gifi.mp3")
      IsPlaying = false
      Position = Left }

let mutable sat =
    { Name = "sat"
      ImageFile = (imageSourceDir + "sat.png")
      Audio = newAudio (audioSourceDir + "saturer.mp3")
      IsPlaying = false
      Position = Middle }

let mutable swing =
    { Name = "swing"
      ImageFile = (imageSourceDir + "elec.png")
      Audio = newAudio (audioSourceDir + "swing.mp3")
      IsPlaying = false
      Position = Right }

let mutable death =
    { Name = "death"
      ImageFile = (imageSourceDir + "death.png")
      Audio = newAudio (audioSourceDir + "death.mp3")
      IsPlaying = false
      Position = Left }

let mutable popo =
    { Name = "popo"
      ImageFile = (imageSourceDir + "médoc-liquide.png")
      Audio = newAudio (audioSourceDir + "popo.mp3")
      IsPlaying = false
      Position = Middle }

let mutable shot =
    { Name = "shot"
      ImageFile = (imageSourceDir + "headshot.png")
      Audio = newAudio (audioSourceDir + "headshot.mp3")
      IsPlaying = false
      Position = Right }

let mutable verre =
    { Name = "verre"
      ImageFile = (imageSourceDir + "verre.png")
      Audio = newAudio (audioSourceDir + "verre.mp3")
      IsPlaying = false
      Position = Left }

let mutable goute =
    { Name = "goute"
      ImageFile = (imageSourceDir + "goute.png")
      Audio = newAudio (audioSourceDir + "eau.mp3")
      IsPlaying = false
      Position = Middle }

let mutable coq =
    { Name = "coq"
      ImageFile = (imageSourceDir + "coq.png")
      Audio = newAudio (audioSourceDir + "coq.mp3")
      IsPlaying = false
      Position = Right }

let mutable poule =
    { Name = "poule"
      ImageFile = (imageSourceDir + "poulet.png")
      Audio = newAudio (audioSourceDir + "poule.mp3")
      IsPlaying = false
      Position = Left }

let mutable fox =
    { Name = "fox"
      ImageFile = (imageSourceDir + "20th_century_fox.png")
      Audio = newAudio (audioSourceDir + "twentyth_century_fox.mp3")
      IsPlaying = false
      Position = Middle }

let mutable engine =
    { Name = "engine"
      ImageFile = (imageSourceDir + "ferrari.png")
      Audio = newAudio (audioSourceDir + "engine.mp3")
      IsPlaying = false
      Position = Right }

let mutable universal =
    { Name = "universal"
      ImageFile = (imageSourceDir + "universal.png")
      Audio = newAudio (audioSourceDir + "universal_studios.mp3")
      IsPlaying = false
      Position = Left }

let mutable mario =
    { Name = "mario"
      ImageFile = (imageSourceDir + "level.png")
      Audio = newAudio (audioSourceDir + "mario.mp3")
      IsPlaying = false
      Position = Middle }

let mutable piece =
    { Name = "piece"
      ImageFile = (imageSourceDir + "piece.png")
      Audio = newAudio (audioSourceDir + "piece.mp3")
      IsPlaying = false
      Position = Right }

let allSoundPacks =
    [ [ sncf; ah; toca ]
      [ tac; bull; sonnette ]
      [ tombo; bol; marche ]
      [ av; non; coucou ]
      [ temp; rob; prout ]
      [ gifi; sat; swing ]
      [ death; popo; shot ]
      [ verre; goute; coq ]
      [ poule; fox; engine ]
      [ universal; mario; piece ] ]


let playOrStopSound (sound: SoundObject) =
    if not sound.IsPlaying then
        sound.IsPlaying <- true
        sound.Audio.play()
        printfn "%s is playing" sound.Name

        sound.Audio.addEventListener("ended", (fun _ ->
            printfn "%s finished" sound.Name
            sound.IsPlaying <- false
        ))
    else
        printfn "%s stopped" sound.Name
        sound.IsPlaying <- false

        //? Stop sound
        sound.Audio.pause()
        sound.Audio.currentTime <- 0.
module Main

open Feliz
open App
open Browser.Dom
open Fable.Core.JsInterop

importAll "./styles/global.sass"

let darkColor = "#141C1F"
let lightColor = "#ffffff"

[<ReactComponent>]
let Board() =
    Html.div [
        prop.children (Sounds.allSoundPacks |> List.map (fun soundPack ->
        Html.div [
            prop.className "button-line"
            prop.children (soundPack |> List.map (fun sound ->
                Components.Button {| sound = sound |}
            ))
        ]
        ))
    ]

[<ReactComponent>]
let Page() =
    let (lightMode, setLightMode) = React.useState(false)
    document.body.classList.add "dark-mode"

    Html.div [
        prop.style [
            style.backgroundColor (
                if lightMode then lightColor
                else darkColor
            )
        ]
        prop.children [
            Components.Switch {|
                value = false
                onPress = (fun value ->
                    setLightMode value

                    document.body.className <- ""
                    if value then
                        document.body.classList.add "light-mode"
                    else
                        document.body.classList.add "dark-mode"
                )
                onColor = lightColor
                offColor = darkColor
            |}

            Html.button [
                prop.className "title"
                prop.children [
                    Html.p [
                        prop.style [
                            style.color (
                                if lightMode then lightColor
                                else darkColor
                            )
                        ]
                        prop.text "SoundBox"
                    ]
                ]
                prop.onClick (fun _ ->
                    Sounds.playOrStopSound Sounds.dejaVu
                )
            ]

            Board()
        ]
    ]

ReactDOM.render(
    Page(),
    document.getElementById "app"
)